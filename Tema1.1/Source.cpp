#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <memory>
#include "Vehicle.h"
#include <conio.h>

auto main() -> int
{
	{
		//std::map<Vehicle, std::string> vehiclesMap;
		std::map<Vehicle, std::string, Vehicle::Comparator>test;

		test.emplace(Vehicle("Dacia", 2010), "Dacia");
		test.emplace(Vehicle("Audi", 1995), "Audi");
		test.emplace(Vehicle("Honda", 2018), "Honda");
		test.emplace(Vehicle("BMW", 2013), "BMW");
		test.emplace(Vehicle("Volvo", 1980), "Volvo");

		for (const auto& element : test)
		{
			std::cout << element.second << '\n';
		}
	}
	/*	Utils<int> lv_nUtils;
		Utils<double> lv_dfUtils;

		auto const lv_nMaxElement = lv_dfUtils.mf_MaxElement(7, 10);
		auto const lv_dfMaxElement = lv_dfUtils.mf_MaxElement(2.3, 4.5);

		std::cout << lv_nMaxElement << std::endl;
		std::cout << lv_dfMaxElement << std::endl << std::endl;
	}

	{
		std::map<int, std::string> lv_Map;
		lv_Map.insert(std::make_pair(5, std::string("FirstInesrtion")));
		lv_Map.insert(std::make_pair(3, std::string("SecondInsertion")));

		for (auto const mapIterator : lv_Map)
		{
			std::cout << mapIterator.first << " " << mapIterator.second << std::endl;
		}

	}

	{
		std::map<Utils<int>, int, Utils<int>::Comparator> lv_UtilsMaps;
		Utils<int> lv_nUtils1;
		Utils<int> lv_nUtils2;
		lv_UtilsMaps.insert(std::make_pair(lv_nUtils1, 0));
		lv_UtilsMaps.insert(std::make_pair(lv_nUtils2, 1));

	}

	std::shared_ptr<Utils<int>> lv_sharedPtr1 = std::make_shared<Utils<int>>();
	{
		std::shared_ptr<Utils<int>> lv_sharedPtr2 = lv_sharedPtr1;
		std::cout << std::endl << "Refference count: " << lv_sharedPtr1.use_count() << std::endl;
	}
	std::cout << "Refference count: " << lv_sharedPtr1.use_count() << std::endl << std::endl;	*/


		_getch();
	return EXIT_SUCCESS;
}